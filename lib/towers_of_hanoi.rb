# Towers of Hanoi
#
# Write a Towers of Hanoi game:
# http://en.wikipedia.org/wiki/Towers_of_hanoi
#
# In a class `TowersOfHanoi`, keep a `towers` instance variable that is an array
# of three arrays. Each subarray should represent a tower. Each tower should
# store integers representing the size of its discs. Expose this instance
# variable with an `attr_reader`.
#
# You'll want a `#play` method. In a loop, prompt the user using puts. Ask what
# pile to select a disc from. The pile should be the index of a tower in your
# `@towers` array. Use gets
# (http://andreacfm.com/2011/06/11/learning-ruby-gets-and-chomp.html) to get an
# answer. Similarly, find out which pile the user wants to move the disc to.
# Next, you'll want to do different things depending on whether or not the move
# is valid. Finally, if they have succeeded in moving all of the discs to
# another pile, they win! The loop should end.
#
# You'll want a `TowersOfHanoi#render` method. Don't spend too much time on
# this, just get it playable.
#
# Think about what other helper methods you might want. Here's a list of all the
# instance methods I had in my TowersOfHanoi class:
# * initialize
# * play
# * render
# * won?
# * valid_move?(from_tower, to_tower)
# * move(from_tower, to_tower)
#
# Make sure that the game works in the console. There are also some specs to
# keep you on the right track:
#
# ```bash
# bundle exec rspec spec/towers_of_hanoi_spec.rb
# ```
#
# Make sure to run bundle install first! The specs assume you've implemented the
# methods named above.
require 'byebug'
class TowersOfHanoi
  attr_reader :towers

  def initialize
    @towers = [[3, 2, 1], [], []]
  end

  def play
    render
    if won?
      puts 'You won!'
      return
    end
    from_tower = entry
    to_tower = entry(from_tower)
    if !valid_move?(from_tower, to_tower)
      puts 'Invalid move!'
    else
      move(from_tower, to_tower)
    end
    play
  end

  def entry(first = nil)
    messages = { tower1: 'Top disc of which tower?',
                 tower2: 'Select a tower to place the disc.',
                 error: 'Wrong selection, try again' }
    valid = [0, 1, 2]
    tower = nil
    loop do
      if first.nil?
        puts messages[:tower1]
      else
        puts messages[:tower2]
      end
      tower = gets.chomp.to_i - 1
      break if valid.include?(tower) && first != tower
      puts messages[:error]
    end
    tower
  end

  def move(from_tower, to_tower)
    @towers[to_tower].push(@towers[from_tower].pop) if valid_move?(from_tower, to_tower)
  end

  def render
    number_disks = 3
    disk_print = { 0 => '   |    ', 1 => '  x|x   ',
                   2 => ' xx|xx  ', 3 => 'xxx|xxx ' }
    (towers.length - 1).step(0, -1).each do |row|
      printer = ''
      number_disks.times do |col|
        disk = towers[col][row]
        disk = disk.nil? ? 0 : disk
        printer << disk_print[disk]
      end
      puts printer
    end
  end

  def won?
    winner = [3, 2, 1]
    @towers[1] == winner || @towers[-1] == winner
  end

  def valid_move?(from_tower, to_tower)
    from_disc = @towers[from_tower][-1]
    return false if from_disc.nil?
    to_disc = @towers[to_tower][-1]
    to_disc.nil? || to_disc > from_disc
  end
end
